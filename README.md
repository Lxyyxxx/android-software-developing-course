# Android Software Developing Course
## 1
  - date: 2019.03.01
  - name: Hello World
  
## 2
  - date: 2019.03.28
  - name: UI

## 3
  - date: 2019.04.03
  - name: 事件

## 4
  - date: 2019.04.12
  - name: 服务

## 5
  - date: 2019.04.18
  - name: 数据存储

## 6
  - date: 2019.04.26
  - name: 网络API

## 7
  - date: 2019.05.04
  - name: 传感器

## 8
  - date: 2019.05.09
  - name: 播放器

## Final
  - date: 2019.05.30
  - name: 大作业