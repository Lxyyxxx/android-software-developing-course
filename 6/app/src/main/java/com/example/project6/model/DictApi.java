package com.example.project6.model;

import com.example.project6.bean.DictReturn;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class DictApi {
    public final static String APP_KEY = "c276e3e12bc6bc4cebe6e23fd239c7cf";
    public final static String DTYPE = "json";
    public final static String BASE_URL = "http://v.juhe.cn/";
    public final static String DICT = "xhzd/query";
    public interface DictService{
        @GET(DICT)
        Observable<DictReturn> getDict(@Query("word")String word, @Query("dtype")String dtype, @Query("key")String app_key);
    }
}

//请求地址：http://v.juhe.cn/xhzd/query
//请求参数：word=(输入的字)&dtype=json&key=(我的app_key)
//请求方式：GET
