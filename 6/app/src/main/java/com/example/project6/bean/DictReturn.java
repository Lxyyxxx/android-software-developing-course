package com.example.project6.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class DictReturn {
    @Getter
    @Setter
    private String reason;
    @Getter
    @Setter
    private DictResult result;
}

// {
//    "reason": "返回成功",
//    "result": {
//        "zi": "聚",
//        "wubi": "bcti",
//        "pinyin": "jù",
//        "bushou": "耳",
//        "bihua": "14"
//    },
//}
