package com.example.project6.contract;

import com.example.project6.bean.DictResult;
import com.example.project6.bean.DictReturn;

public interface DictContract {
    interface DictModelInterface{
        void getDict(String word, DictCallback dictCallback);
        interface DictCallback{
            void onDict(DictReturn dictReturn);
        }
    }

    interface DictPresenterInterface{
        void getDict(String word);
    }

    interface DictViewInterface{
        void showZi(DictResult dictResult);
        void showWubi(DictResult dictResult);
        void showPinyin(DictResult dictResult);
        void showBushou(DictResult dictResult);
        void showBihua(DictResult dictResult);
    }
}
