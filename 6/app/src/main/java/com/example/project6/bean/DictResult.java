package com.example.project6.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
public class DictResult {
    @Getter
    @Setter
    private String zi; //字
    @Getter
    @Setter
    private String wubi; //五笔
    @Getter
    @Setter
    private String pinyin; //拼音
    @Getter
    @Setter
    private String bushou; //部首
    @Getter
    @Setter
    private String bihua; //笔画
}

// {
//    "reason": "返回成功",
//    "result": {
//        "zi": "聚",
//        "wubi": "bcti",
//        "pinyin": "jù",
//        "bushou": "耳",
//        "bihua": "14"
//    },
//}
