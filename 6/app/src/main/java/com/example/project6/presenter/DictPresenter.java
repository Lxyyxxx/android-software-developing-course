package com.example.project6.presenter;

import com.example.project6.base.BasePresenter;
import com.example.project6.bean.DictReturn;
import com.example.project6.contract.DictContract;
import com.example.project6.model.DictModel;

public class DictPresenter extends BasePresenter<DictContract.DictViewInterface> implements DictContract.DictPresenterInterface {

    DictModel dictModel;
    DictContract.DictModelInterface.DictCallback dictCallback = new DictContract.DictModelInterface.DictCallback() {
        @Override
        public void onDict(DictReturn dictReturn) {
            getView().showZi(dictReturn.getResult());
            getView().showWubi(dictReturn.getResult());
            getView().showPinyin(dictReturn.getResult());
            getView().showBushou(dictReturn.getResult());
            getView().showBihua(dictReturn.getResult());
        }
    };

    public DictPresenter(DictContract.DictViewInterface v) {
        super(v);
        dictModel=new DictModel();
    }

    @Override
    public void getDict(String word) {
        dictModel.getDict(word, dictCallback);
    }

}
