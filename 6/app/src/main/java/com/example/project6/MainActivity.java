package com.example.project6;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.project6.base.BaseActivity;
import com.example.project6.bean.DictResult;
import com.example.project6.contract.DictContract;
import com.example.project6.presenter.DictPresenter;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity<DictContract.DictViewInterface, DictPresenter> implements DictContract.DictViewInterface {

    DictPresenter dictPresenter;

    @BindView(R.id.input)
    EditText editText;

    @BindView(R.id.showz)
    TextView textViewZ;
    @BindView(R.id.showwb)
    TextView textViewWb;
    @BindView(R.id.showpy)
    TextView textViewPy;
    @BindView(R.id.showbh)
    TextView textViewBh;
    @BindView(R.id.showbs)
    TextView textViewBs;

    @OnClick(R.id.btn)
    public void ClickBtn(){
        String input = editText.getText().toString();
        Pattern pattern = Pattern.compile("^[\\u4E00-\\u9FA5]{1}$");
        Matcher matcher = pattern.matcher(input);
        if(input.length()!=1 || !matcher.matches()){
            Toast.makeText(MainActivity.this,"请输入一个汉字",Toast.LENGTH_SHORT).show();
        }else{
            dictPresenter = getPresenter();
            dictPresenter.getDict(input);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }

    @Override
    protected DictPresenter createPresenter() {
        return new DictPresenter(this);
    }

    @Override
    public void showZi(DictResult dictResult){
        textViewZ.setText("查询的字：" + dictResult.getZi());
    }

    @Override
    public void showWubi(DictResult dictResult){
        textViewWb.setText("五笔：" + dictResult.getWubi());
    }

    @Override
    public void showPinyin(DictResult dictResult){
        textViewPy.setText("拼音：" + dictResult.getPinyin());
    }

    @Override
    public void showBushou(DictResult dictResult){
        textViewBs.setText("部首：" + dictResult.getBushou());
    }

    @Override
    public void showBihua(DictResult dictResult){
        textViewBh.setText("笔画：" + dictResult.getBihua());
    }
}
