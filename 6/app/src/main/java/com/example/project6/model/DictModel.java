package com.example.project6.model;

import android.util.Log;

import com.example.project6.bean.DictReturn;
import com.example.project6.contract.DictContract;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.fastjson.FastJsonConverterFactory;


public class DictModel implements DictContract.DictModelInterface {

    DictApi.DictService dictService;

    public DictModel(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(DictApi.BASE_URL)
                .addConverterFactory(FastJsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        dictService = retrofit.create(DictApi.DictService.class);
    }

    @Override
    public void getDict(String word, final DictCallback dictCallback) {
        dictService.getDict(word, DictApi.DTYPE, DictApi.APP_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableObserver<DictReturn>() {
                    @Override
                    public void onNext(DictReturn dictReturn) {
                        if (dictReturn.getReason().equalsIgnoreCase("返回成功")) {
                            Log.d("DictModel:", "“" + dictReturn.getResult().getZi() + "”查询成功");
                            dictCallback.onDict(dictReturn);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
