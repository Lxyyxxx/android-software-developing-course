package com.example.project2;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.view.Gravity;
import android.widget.LinearLayout;

public class UC1 extends Activity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uc1);
        findViewById(R.id.uc1_turn_back).setOnClickListener(this);
        findViewById(R.id.uc1_btn1).setOnClickListener(this);
        findViewById(R.id.uc1_btn2).setOnClickListener(this);
        findViewById(R.id.uc1_btn3).setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        Intent intent=new Intent();
        LinearLayout ll=findViewById(R.id.uc1);
        switch (view.getId()){
            case R.id.uc1_turn_back:
                intent.setClass(this,MainActivity.class);
                startActivity(intent);
                break;
            case R.id.uc1_btn1:
                ll.setOrientation(LinearLayout.HORIZONTAL);
                ll.setGravity(Gravity.CENTER_HORIZONTAL);
                break;
            case R.id.uc1_btn2:
                ll.setOrientation(LinearLayout.VERTICAL);
                ll.setGravity(Gravity.CENTER_HORIZONTAL);
                break;
            case R.id.uc1_btn3:
                ll.setGravity(Gravity.LEFT);
                break;
        }
    }

}
