package com.example.project2;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;

public class MainActivity extends Activity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewById(R.id.btn_uc1).setOnClickListener(this);
        findViewById(R.id.btn_uc2).setOnClickListener(this);
        findViewById(R.id.btn_uc3).setOnClickListener(this);
        findViewById(R.id.btn_uc4).setOnClickListener(this);
        findViewById(R.id.btn_uc5).setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        Intent intent=new Intent();
        switch(view.getId()){
            case R.id.btn_uc1:
                intent.setClass(this,UC1.class);
                startActivity(intent);
                break;
            case R.id.btn_uc2:
                intent.setClass(this,UC2.class);
                startActivity(intent);
                break;
            case R.id.btn_uc3:
                intent.setClass(this,UC3.class);
                startActivity(intent);
                break;
            case R.id.btn_uc4:
                intent.setClass(this,UC4.class);
                startActivity(intent);
                break;
            case R.id.btn_uc5:
                intent.setClass(this,UC5.class);
                startActivity(intent);
                break;
        }
    }

}
