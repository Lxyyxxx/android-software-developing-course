package com.example.project2;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;

public class UC4 extends Activity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.uc4);
        findViewById(R.id.uc4_turn_back).setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        Intent intent=new Intent();
        if(view.getId()==R.id.uc4_turn_back) {
            intent.setClass(this,MainActivity.class);
            startActivity(intent);
        }
    }

}