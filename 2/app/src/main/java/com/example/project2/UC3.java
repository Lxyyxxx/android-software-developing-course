package com.example.project2;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

public class UC3 extends Activity implements View.OnClickListener{

    private int currentImg = 0;

    final ImageView [] imageViews=new ImageView[5];

    final int [] images = new int[]{
            R.id.uc3_img1,
            R.id.uc3_img2,
            R.id.uc3_img3,
            R.id.uc3_img4,
            R.id.uc3_img5
    };

    final int [] draws = new int[]{
            R.drawable.a,
            R.drawable.b,
            R.drawable.c,
            R.drawable.d,
            R.drawable.e
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.uc3);

        for (int i=0;i<5;i++)
        {
            imageViews[i]=findViewById(images[i]);
        }

        final Handler handler = new Handler(){

            @Override
            public void handleMessage(Message msg) {

                if (msg.what==0x123) {
                    currentImg ++;
                    if(currentImg>=4){
                        currentImg=0;
                    }
                    for (int i=0;i<5;i++) {
                        imageViews[i].setImageResource(draws[(i+currentImg)%5]);
                    }
                }
                super.handleMessage(msg);
            }

        };

        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.sendEmptyMessage(0x123);
            }

        }, 0, 3000);

        findViewById(R.id.uc3_turn_back).setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        Intent intent=new Intent();
        if(view.getId()==R.id.uc3_turn_back) {
            intent.setClass(this,MainActivity.class);
            startActivity(intent);
        }

    }

}
