package com.example.helloworld;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    // onCreate 当活动第一次被创建时调用
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); //当一个Activity在生命周期结束前，会调用saveInstanceState方法方法保存Activity的状态。
        setContentView(R.layout.activity_main); //引用res(R)/layout/activity_main.xml
    }
}
