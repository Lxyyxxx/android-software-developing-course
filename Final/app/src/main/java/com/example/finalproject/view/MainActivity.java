package com.example.finalproject.view;

import android.Manifest;
import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.example.finalproject.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path ="/app/main/view/MainActivity")
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ARouter.openDebug();
        ARouter.openLog();
        ARouter.init(getApplication());
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);
        }

    }
    @OnClick(R.id.register)
    public void goRegister(){
        ARouter.getInstance().build("/app/main/view/Register").navigation();
    }
    @OnClick(R.id.check_in)
    public void goCheckIn(){
        ARouter.getInstance().build("/app/main/view/CheckIn").navigation();
    }

}
