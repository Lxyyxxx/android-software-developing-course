package com.example.finalproject.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.example.finalproject.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.OnClick;

@Route(path = "/app/main/view/Register")
public class Register extends AppCompatActivity {

    //private static final String PATH = "/sdcard/Pictures";
    private static final String PATH = Environment.getExternalStorageDirectory().getPath()+"/userPhoto";
    private int REQUEST_CAPTURE_IMAGE = 0;
    private Bitmap photoBit;
    private ImageView add_face;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Log.d("打开","初始化");
        add_face = (ImageView) findViewById(R.id.photo);
        add_face.setBackgroundResource(R.drawable.tianjia);
        add_face.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("打开相机","开始");
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if(intent.resolveActivity(getPackageManager())!=null){
                    Log.d("开始拍照","开始");
                    startActivityForResult(intent,REQUEST_CAPTURE_IMAGE);
                }
                savePhotoToStorage(PATH,"userFace.jpg",photoBit);
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(resultCode == RESULT_OK){
            if(requestCode == REQUEST_CAPTURE_IMAGE){
                Toast.makeText(this,"拍照成功",Toast.LENGTH_LONG).show();
                Bundle extras = data.getExtras();
                photoBit = (Bitmap) extras.get("data");
                add_face.setImageBitmap(photoBit);
            }
        }else {
            Toast.makeText(Register.this,"获取资源失败",Toast.LENGTH_LONG).show();
            return;
        }
    }

    public static void savePhotoToStorage(String path,String photeName,Bitmap photoBitmap){
        if(android.os.Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            File dir = new File(PATH);
            Log.d("照片保存路径",PATH);
            if(!dir.exists()){
                dir.mkdirs();
            }
            File photoFile = new File(PATH,photeName);
            Log.d("照片文件",photoFile+"");
            FileOutputStream fileOutputStream = null;
            try{
                fileOutputStream = new FileOutputStream(photoFile);
                Log.d("fileOutputstream",fileOutputStream+"");
                if(photoBitmap != null){
                    Log.d("存储","照片");
                    if(photoBitmap.compress(Bitmap.CompressFormat.JPEG,100,fileOutputStream)){
                        fileOutputStream.flush();;
                        fileOutputStream.close();
                    }
                }
            } catch (FileNotFoundException e) {
                photoFile.delete();
                e.printStackTrace();
            } catch (IOException e) {
                photoFile.delete();
                e.printStackTrace();
            }finally {
                try {
                    fileOutputStream.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
