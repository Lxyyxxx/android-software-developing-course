package com.example.finalproject.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.example.finalproject.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path = "/app/main/view/CheckIn")
public class CheckIn extends AppCompatActivity {

    private int REQUEST_CAPTURE_IMAGE = 0;
    private Bitmap get_face;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.return_Main)
    public void returnMain(){
        Log.d("返回","开始");
        ARouter.getInstance().build("/app/main/view/MainActivity").navigation();
    }

    @OnClick(R.id.start_camera)
    public void startCamera(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if(intent.resolveActivity(getPackageManager())!=null){
            Log.d("开始拍照","开始");
            startActivityForResult(intent,REQUEST_CAPTURE_IMAGE);
        }
        //!Todo 这里对获取到的照片get_face调取API对比
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode, Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(resultCode == RESULT_OK){
            if(requestCode == REQUEST_CAPTURE_IMAGE){
                Toast.makeText(this,"拍照成功",Toast.LENGTH_LONG).show();
                Bundle extras = data.getExtras();
                get_face= (Bitmap) extras.get("data");
            }
        }else {
            Toast.makeText(CheckIn.this,"获取资源失败",Toast.LENGTH_LONG).show();
            return;
        }
    }

}
