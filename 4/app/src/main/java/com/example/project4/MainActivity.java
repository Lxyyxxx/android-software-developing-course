package com.example.project4;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //防止点击两次解绑会崩溃
    private boolean isBind=false;
    Button bind, unbind, getServiceStatus;
    // 保持所启动的Service的IBinder对象
    BindService.MyBinder binder;
    // 定义一个ServiceConnection对象
    private ServiceConnection conn = new ServiceConnection(){
        // 当该Activity与Service连接成功时回调该方法
        @Override
        public void onServiceConnected(ComponentName name, IBinder service){
            binder=(BindService.MyBinder)service;
            System.out.println("Service连接成功");
        }
        // 当该Activity与Service断开连接时回调该方法
        // onServiceDisconnected()在连接正常关闭的情况下是不会被调用的，该方法只在Service被破坏了或者被杀死的时候调用。
        @Override
        public void onServiceDisconnected(ComponentName name){
            System.out.println("Service断开连接");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // 获取程序界面中的start、stop、getServiceStatus按钮
        bind = (Button) findViewById(R.id.bind);
        unbind = (Button) findViewById(R.id.unbind);
        getServiceStatus = (Button) findViewById(R.id.getServiceStatus);
        //创建启动Service的Intent
        final Intent intent = new Intent();
        //为Intent设置Action属性
        intent.setAction("com.example.project4.BindService");
        //防止报错Service Intent must be explicit
        intent.setPackage(getPackageName());
        bind.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View source){
                //绑定指定Service
                if(!isBind){
                    bindService(intent, conn, Service.BIND_AUTO_CREATE);
                    Toast.makeText(MainActivity.this,"Service绑定成功",Toast.LENGTH_SHORT).show();
                    isBind=true;
                }
                else{
                    Toast.makeText(MainActivity.this,"Service已绑定",Toast.LENGTH_SHORT).show();
                }
            }
        });
        unbind.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View source){
                //解除绑定Service
                if(isBind){
                    unbindService(conn);
                    isBind=false;
                    Toast.makeText(MainActivity.this,"Service解绑成功",Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(MainActivity.this,"Service未绑定，解除绑定失败",Toast.LENGTH_SHORT).show();
                }
            }
        });
        getServiceStatus.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View source){
                // 获取、并显示Service的count值
                if(isBind){
                    Toast.makeText(MainActivity.this, "Service的count值为：" + binder.getCount(), Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(MainActivity.this,"Service未绑定，获取Service状态失败", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
