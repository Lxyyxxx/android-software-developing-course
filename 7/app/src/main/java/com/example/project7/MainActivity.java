package com.example.project7;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.alibaba.android.arouter.launcher.ARouter;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ARouter.openLog();
        ARouter.openDebug();
        ARouter.init(getApplication());
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btn_magnetic,R.id.btn_orientation,R.id.btn_pressure,R.id.btn_gyroscope,R.id.btn_temperature,R.id.btn_light})
    void SensorChoose(View view){
        switch ((view.getId())){
            case R.id.btn_magnetic:
                ARouter.getInstance().build("/app/magnetic").navigation();
                break;
            case R.id.btn_orientation:
                ARouter.getInstance().build("/app/orientation").navigation();
                break;
            case R.id.btn_pressure:
                ARouter.getInstance().build("/app/pressure").navigation();
                break;
            case R.id.btn_gyroscope:
                ARouter.getInstance().build("/app/gyroscope").navigation();
                break;
            case R.id.btn_temperature:
                ARouter.getInstance().build("/app/temperature").navigation();
                break;
            case R.id.btn_light:
                ARouter.getInstance().build("/app/light").navigation();
                break;
        }
    }
}
