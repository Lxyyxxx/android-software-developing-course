package com.example.project8;


import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.alibaba.android.arouter.launcher.ARouter;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ARouter.openLog();
        ARouter.openDebug();
        ARouter.init(getApplication());
        ButterKnife.bind(this);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},1);
        }
    }

    @BindView(R.id.input)
    EditText editText;

    @OnClick(R.id.btn)
    void myClick(){
        String url = editText.getText().toString();
        File file = new File(Environment.getExternalStorageDirectory() + url);
        if(file.exists()){
            String myURL = file.getPath();
            ARouter.getInstance().build("/app/player").withString("myURL",myURL).navigation();
        }else{
            Toast.makeText(MainActivity.this,"文件不存在",Toast.LENGTH_SHORT).show();
        }
    }

}
