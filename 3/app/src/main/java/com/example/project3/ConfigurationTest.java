package com.example.project3;

import android.os.Bundle;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.alibaba.android.arouter.facade.annotation.Route;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path="/app/ConfigurationTest")
public class ConfigurationTest extends AppCompatActivity {

    @BindView(R.id.ori) EditText ori;
    @BindView(R.id.navigation) EditText navigation;
    @BindView(R.id.touch) EditText touch;
    @BindView(R.id.mnc) EditText mnc;

    @OnClick(R.id.bn)
    public void bnClick(){
        // 获取系统的Configuration对象
        Configuration cfg = getResources().getConfiguration();
        //屏幕方向
        String screen = cfg.orientation == Configuration.ORIENTATION_LANDSCAPE ? "横向屏幕": "竖向屏幕";
        //手机方向控制设备
        String naviName = cfg.orientation == Configuration.NAVIGATION_NONAV ? "没有方向控制": cfg.orientation == Configuration.NAVIGATION_WHEEL ? "滚轮控制方向": cfg.orientation == Configuration.NAVIGATION_DPAD ? "方向键控制方向": "轨迹球控制方向";
        //触摸屏状态
        String touchName = cfg.touchscreen == Configuration.TOUCHSCREEN_NOTOUCH ? "无触摸屏": cfg.touchscreen == Configuration.TOUCHSCREEN_STYLUS ? "触摸笔式触摸屏": "接受手指的触摸屏";
        //移动网络代号
        String mncCode = cfg.mnc + "";

        ori.setText(screen);
        navigation.setText(naviName);
        touch.setText(touchName);
        mnc.setText(mncCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuration_test);
        ButterKnife.bind(this);
    }
}
