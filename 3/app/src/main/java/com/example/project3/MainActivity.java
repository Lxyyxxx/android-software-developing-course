package com.example.project3;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

@Route(path="/app/MainActivity")
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @OnClick({R.id.btn7,R.id.btn8})
    public void myClick(View view) {
        switch (view.getId()) {

            //系统信息
            case R.id.btn7:
                ARouter.getInstance().build("/app/ConfigurationTest").navigation();
                break;

            //模拟进度条
            case R.id.btn8:
                ARouter.getInstance().build("/app/ProgressDialogTest").navigation();
                break;
        }
    }

    @BindView(R.id.text) TextView textView;

    @OnClick(R.id.btn6)
    public void ClickBtn6(){
        textView.setText("点击了采用ButterKnife绑定的监听器");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Activity作为监听器
        findViewById(R.id.btn1).setOnClickListener(this);

        //内部匿名类
        findViewById(R.id.btn2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText("点击了采用匿名内部类作为监听器的监听器");
            }
        });

        //内部类
        findViewById(R.id.btn4).setOnClickListener(new InnerListener());

        //外部类
        TextView tV=findViewById(R.id.text);
        findViewById(R.id.btn5).setOnClickListener(new OuterListener(tV));

        //ARouter初始化
        ARouter.openLog();
        ARouter.openDebug();
        ARouter.init(getApplication());

        //ButterKnife绑定
        ButterKnife.bind(this);
    }

    //内部类
    class InnerListener implements View.OnClickListener{
        @Override
        public void onClick(View view){
            textView.setText("点击了采用内部类作为监听器的监听器");
        }
    }

    //Activity作为监听器
    @Override
    public void onClick(View view) {
        textView.setText("点击了采用Activity作为监听器的监听器");
    }

    //绑定到标签
    public void buttonClick(View view){
        textView.setText("点击了采用绑定到标签的监听器");
    }

}

//外部类
class OuterListener implements View.OnClickListener {

    private TextView textView;

    OuterListener(TextView tv) {
        textView = tv;
    }

    @Override
    public void onClick(View view){
        textView.setText("点击了采用外部类作为监听器的监听器");
    }

}
